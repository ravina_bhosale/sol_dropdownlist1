﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_DropDownList_With_TextBox
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected async void btnSubmit_Click(object sender, EventArgs e)
        {
            await this.ColorDropDownListBind();
        }

        #region Private Methods
        private async Task ColorDropDownListBind()
        {
            await Task.Run(() =>
            {

                if (txtColors.Text.Trim() != "")
                {
                    ddlColors.Items.Add(new ListItem(txtColors.Text, txtColors.Text));
                }

            });
        }
        #endregion
    }
}