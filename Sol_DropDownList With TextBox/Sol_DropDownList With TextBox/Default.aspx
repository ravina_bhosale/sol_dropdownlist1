﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_DropDownList_With_TextBox.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="scriptManager" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtColors" runat="server"></asp:TextBox> 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" Text="Add" OnClick="btnSubmit_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="ddlColors" runat="server">
                                   <asp:ListItem Text="--Select Colors--"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
